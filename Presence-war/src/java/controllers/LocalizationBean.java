/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author Björn
 */
@ManagedBean
@SessionScoped
public class LocalizationBean {
    
    private String locale="en";
    
    public String getLocale () {return locale;}
    public void setLocale(String loc) {locale=loc;}
    
    private static Map<String, Object> locales;
    
    // run to update the list given to the .jsf request
    private void updateLocales()
    {
        locales = new LinkedHashMap<String,Object>();
        Locale l = Locale.forLanguageTag(locale);
        ResourceBundle labels = ResourceBundle.getBundle("controllers.Messages", l);

        locales.put(labels.getString("English"), Locale.ENGLISH);
        locales.put(labels.getString("Swedish"), Locale.forLanguageTag("sv-SE"));
    }
    
    public Map<String, Object> getLocales()
    {
            updateLocales();
        return locales;
    }
    public void setLocales(Map<String, Object> map)
    {
        return;
    }
    
    public void localeChanged(ValueChangeEvent e) {
        setLocale(e.getNewValue().toString());
//        locale=FacesContext.getCurrentInstance().get
// Map<String,String> params = 
//                FacesContext.getExternalContext().getRequestParameterMap();
//	  String action = params.get("action");
    }
    
}
