/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Models.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;





/**
 *
 * @author Elev1*/
@ManagedBean
@SessionScoped
public class AddDeleteStudent {


    private boolean updateMode;
    private Student student;
    List<Student> students;
    public Student getStudent() {
        
        if(this.student == null) {
         this.student = new Student();  
        }
        return this.student;
    }
    
    public void setUpdateMode(boolean u) {
        this.updateMode = u;

       }
    
    
    public boolean getUpdateMode() {
        return this.updateMode;
    }
    
    public String addStudent() {
    if(this.student != null) {
        this.students.add(this.student);
        this.student = new Student();
    }
    
    return "addDeleteStudent";

}   
    
    
    public String reset() {
        this.student = new Student();
        return "addDeleteStudent";
    }
    
    
    
    public String cancelUpdate() {
        this.student = new Student();
        this.updateMode = false;
        return "addDeleteStudent";
    }
    
    public String updateStudent(String name) {
        
        Student s = new Student();
        s.setFirstName(name);
        int index = this.students.indexOf(s);
        if(index != -1) {
        this.student = this.students.get(index);
        this.updateMode = true;
        }
        return "addDeleteStudent";
    }
    
    public String saveUpdateStudent() { 
       // this.students.remove(this.student); 
       int index = this.students.indexOf(this.student);
       //this.students.remove(this.student);
       this.students.set(index, this.student);
       this.updateMode = false;
       this.student = new Student();
       return "addDeleteStudent";
    }
    
    
    public String deleteStudent(String firstname) {
       
        Student s = new Student();
        s.setFirstName(firstname);
        this.students.remove(s);
       return "addDeleteStudent";
    }

    
    public void setStudent(Student s) {
        this.student = student;
        
    }
    public AddDeleteStudent() {
        super();
        this.student = null;
        students = new ArrayList<>();
        //students.add("Nils Nilsson");
        //students.add("Kurt Kurtsson");
        //students.add("Ove Ovesson");
    }
    public List<Student> getStudents() {
        return students;
    }
    
    
   
}
    
    

