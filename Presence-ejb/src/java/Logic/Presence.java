/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import Models.Teacher;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author Elev1
 */
public class Presence {
   
    @PostConstruct
    public void onStartup() {
        System.err.println("Initialization successful.");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Presence-ejbPU");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();

        Teacher MrT = new Teacher();
        MrT.setFirstName("Theodore");
        MrT.setLastName("X. Wilkins");
        MrT.setSex("Man");
        MrT.setBirth(new Date("1968-06-01"));
        MrT.setEmail("pitythefool@ateam.org");
        MrT.setPhonenr("555-5555");
        MrT.setAddressCity("Los Angeles");
        MrT.setAddressCountry("United States of America");
        MrT.setAddressPostalCode("40923");
        MrT.setAddressStreet("Santa Monica Boulevard 35");
        MrT.setEmploymentCode("Awsome");
//CREATE
        et.begin();
        em.persist(MrT);
        et.commit();
//UPDATE
        MrT.setLastName("Bertram");
        et.begin();
        em.merge(MrT);
        et.commit();
// FIND        
        et.begin();
        em.find(Teacher.class,MrT.getId());
        et.commit();
// DELETE       
         et.begin();
        em.remove(MrT);
        et.commit();
    }
}
