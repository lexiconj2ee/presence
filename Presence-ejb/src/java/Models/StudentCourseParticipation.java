/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Björn Ros
 */
@Entity
public class StudentCourseParticipation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private boolean active;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name="FK_STUDENT_ID", nullable=false)
    private Student student;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name="FK_COURSE_ID", nullable=false)
    private Course course;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public boolean isActive() {
        return active;
    }

    public Student getStudent() {
        return student;
    }

    public Course getCourse() {
        return course;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentCourseParticipation)) {
            return false;
        }
        StudentCourseParticipation other = (StudentCourseParticipation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Presence.Models.StudentCourseParticipation[ id=" + id + " ]";
    }
    
}
