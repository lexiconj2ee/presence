/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Björn Ros
 */
@Entity
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @NotNull
    @Column(name="COURSE_LANGUAGE", length=30, nullable=false)
    private String language;
    
    @NotNull
    @OneToOne
    @JoinColumn(name="FK_TEACHER_ID", nullable=false)
    private Student mainTeacher;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getLanguage() {
        return language;
    }

    public Student getMainTeacher() {
        return mainTeacher;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setMainTeacher(Student mainTeacher) {
        this.mainTeacher = mainTeacher;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Presence.Models.Course[ id=" + id + " ]";
    }
    
}
