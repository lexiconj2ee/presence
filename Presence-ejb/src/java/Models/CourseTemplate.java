/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Björn Ros
 */
@Entity
public class CourseTemplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(name="NAME", length=50, nullable=false)
    private String name;

    @NotNull
    @Column(name="COURSE_CODE", length=15, nullable=false)
    private String courseCode;
    
    @NotNull
    @Column(name="COURSE_LENGTH_DAYS", nullable=false)
    private int courseLengthDays;
    
    @NotNull
    @Column(name="LEVEL",length=20, nullable=false)
    private String level;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public int getCourseLengthDays() {
        return courseLengthDays;
    }

    public String getLevel() {
        return level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public void setCourseLengthDays(int courseLengthDays) {
        this.courseLengthDays = courseLengthDays;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CourseTemplate)) {
            return false;
        }
        CourseTemplate other = (CourseTemplate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Presence.Models.CourseTemplate[ id=" + id + " ]";
    }
    
}
