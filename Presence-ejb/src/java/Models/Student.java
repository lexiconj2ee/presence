/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.DATE;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Björn Ros
 */
@Entity
public class Student implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }
    
    @NotNull
    @Column(name="FIRST_NAME", length = 40, nullable=false)
    private String firstName;
    
    @NotNull
    @Column(name="LAST_NAME", length = 60, nullable=false)
    private String lastName;
    
    @NotNull
    @Column(name="PHONENR", length = 20, nullable=false)
    private String phonenr;
    
    @NotNull
    @Column(name="EMAIL", length = 255, nullable=false)
    private String email;
    
    @NotNull
    @Column(name="ADDR_STREET", length = 150, nullable=false)
    private String addressStreet;
    
    @NotNull
    @Column(name="ADDR_CITY", length = 60, nullable=false)
    private String addressCity;
    
    @NotNull
    @Column(name="ADDR_POSTALCODE", length = 10, nullable=false)
    private String addressPostalCode;
    
    @NotNull
    @Column(name="ADDR_COUNTRY", length = 30, nullable=false)
    private String addressCountry;
    
    @NotNull
    @Column(name="PERSON_NUMMER", length = 11, nullable=false)
    private String personNummer;
    
    @Column(name="NOTE", length = 2000, nullable=true)
    private String note;
    
    @NotNull
    @Column(name="SEX", length = 5, nullable=false)
    private String sex;
    
    @NotNull
    @Temporal(DATE)
    @Column(name="BIRTH", nullable=false)
    private Date birth;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhonenr() {
        return phonenr;
    }

    public String getEmail() {
        return email;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressPostalCode() {
        return addressPostalCode;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

   

    public String getNote() {
        return note;
    }

    public String getSex() {
        return sex;
    }

    public Date getBirth() {
        return birth;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhonenr(String phonenr) {
        this.phonenr = phonenr;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public void setAddressPostalCode(String addressPostalCode) {
        this.addressPostalCode = addressPostalCode;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getPersonNummer() {
        return personNummer;
    }

    public void setPersonNummer(String personNummer) {
        this.personNummer = personNummer;
    }

  

    public void setNote(String note) {
        this.note = note;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProjectPresence.Teacher[ id=" + id + " ]";
    }
    
}
