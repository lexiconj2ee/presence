/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Models.Teacher;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author Elev1
 */
public class TeacherTest {
    private static EJBContainer ejbContainer;
    private static Context ctx; 
    Teacher instance ;
    
    public TeacherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        ejbContainer = EJBContainer.createEJBContainer();
        System.out.println("Starting the container");
        ctx = ejbContainer.getContext();
    }
    
    @AfterClass
    public static void tearDownClass() {
         ejbContainer.close();
         System.out.println("Closing the container");
    }
    
    @Before
    public void setUp() throws NamingException {
    instance = (Teacher) ctx.lookup("java:global/classes/Teacher");//new Teacher();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Teacher.
     */
    @Test
    public void testGetId() throws NamingException {
        System.out.println("getId");
        
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSerialVersionUID method, of class Teacher.
     */
    @Test
    public void testGetSerialVersionUID() {
        System.out.println("getSerialVersionUID");
        long expResult = 0L;
        long result = Teacher.getSerialVersionUID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFirstName method, of class Teacher.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
       
        String expResult = "";
        String result = instance.getFirstName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLastName method, of class Teacher.
     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");
      
        String expResult = "";
        String result = instance.getLastName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPhonenr method, of class Teacher.
     */
    @Test
    public void testGetPhonenr() {
        System.out.println("getPhonenr");
       
        String expResult = "";
        String result = instance.getPhonenr();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEmail method, of class Teacher.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
    
        String expResult = "";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAddressStreet method, of class Teacher.
     */
    @Test
    public void testGetAddressStreet() {
        System.out.println("getAddressStreet");
     
        String expResult = "";
        String result = instance.getAddressStreet();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAddressCity method, of class Teacher.
     */
    @Test
    public void testGetAddressCity() {
        System.out.println("getAddressCity");
     
        String expResult = "";
        String result = instance.getAddressCity();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAddressPostalCode method, of class Teacher.
     */
    @Test
    public void testGetAddressPostalCode() {
        System.out.println("getAddressPostalCode");
      
        String expResult = "";
        String result = instance.getAddressPostalCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAddressCountry method, of class Teacher.
     */
    @Test
    public void testGetAddressCountry() {
        System.out.println("getAddressCountry");
  
        String expResult = "";
        String result = instance.getAddressCountry();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEmploymentCode method, of class Teacher.
     */
    @Test
    public void testGetEmploymentCode() {
        System.out.println("getEmploymentCode");
 
        String expResult = "";
        String result = instance.getEmploymentCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNote method, of class Teacher.
     */
    @Test
    public void testGetNote() {
        System.out.println("getNote");

        String expResult = "";
        String result = instance.getNote();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSex method, of class Teacher.
     */
    @Test
    public void testGetSex() {
        System.out.println("getSex");

        String expResult = "";
        String result = instance.getSex();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBirth method, of class Teacher.
     */
    @Test
    public void testGetBirth() {
        System.out.println("getBirth");

        Date expResult = null;
        Date result = instance.getBirth();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFirstName method, of class Teacher.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");
        String firstName = "";
  
        instance.setFirstName(firstName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLastName method, of class Teacher.
     */
    @Test
    public void testSetLastName() {
        System.out.println("setLastName");
        String lastName = "";

        instance.setLastName(lastName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPhonenr method, of class Teacher.
     */
    @Test
    public void testSetPhonenr() {
        System.out.println("setPhonenr");
        String phonenr = "";

        instance.setPhonenr(phonenr);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmail method, of class Teacher.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "";

        instance.setEmail(email);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAddressStreet method, of class Teacher.
     */
    @Test
    public void testSetAddressStreet() {
        System.out.println("setAddressStreet");
        String addressStreet = "";

        instance.setAddressStreet(addressStreet);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAddressCity method, of class Teacher.
     */
    @Test
    public void testSetAddressCity() {
        System.out.println("setAddressCity");
        String addressCity = "";

        instance.setAddressCity(addressCity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAddressPostalCode method, of class Teacher.
     */
    @Test
    public void testSetAddressPostalCode() {
        System.out.println("setAddressPostalCode");
        String addressPostalCode = "";

        instance.setAddressPostalCode(addressPostalCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAddressCountry method, of class Teacher.
     */
    @Test
    public void testSetAddressCountry() {
        System.out.println("setAddressCountry");
        String addressCountry = "";

        instance.setAddressCountry(addressCountry);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmploymentCode method, of class Teacher.
     */
    @Test
    public void testSetEmploymentCode() {
        System.out.println("setEmploymentCode");
        String employmentCode = "";

        instance.setEmploymentCode(employmentCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNote method, of class Teacher.
     */
    @Test
    public void testSetNote() {
        System.out.println("setNote");
        String note = "";

        instance.setNote(note);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSex method, of class Teacher.
     */
    @Test
    public void testSetSex() {
        System.out.println("setSex");
        String sex = "";

        instance.setSex(sex);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBirth method, of class Teacher.
     */
    @Test
    public void testSetBirth() {
        System.out.println("setBirth");
        Date birth = null;

        instance.setBirth(birth);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setId method, of class Teacher.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;

        instance.setId(id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hashCode method, of class Teacher.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");

        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class Teacher.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;

        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Teacher.
     */
    @Test
    public void testToString() {
        System.out.println("toString");

        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
